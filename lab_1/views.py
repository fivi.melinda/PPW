from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Fivi Melinda' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 7, 13) #TODO Implement this, format (Year, Month, Date)
npm = 1706984594 # TODO Implement this
faculty = 'Computer Science'
major = 'Information System'
univ = 'Universitas Indonesia'
hobby = 'Listening to music'
desc = "I don't know how to describe myself. But if you want to know more about me..."

#Kiri
teman1 = 'Laila Saffanah'
teman2 = 'Shania Nabilah'
umur1 = date(1999, 11, 2)
umur2 = date(1999, 6, 9)
npm1 = 1706043531
npm2 = 1706044175
desc1 = 'Animation concept enthusiast'
desc2 = 'I love swimming'
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year),
    'npm': npm, 'faculty': faculty, 'major': major, 'univ': univ,
    'hobby': hobby, 'desc': desc, 'teman1': teman1, 'teman2':teman2,
    'umur1': calculate_age(umur1.year), 'umur2': calculate_age(umur2.year), 'npm1': npm1, 'npm2': npm2, 'desc1': desc1,
    'desc2': desc2}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
